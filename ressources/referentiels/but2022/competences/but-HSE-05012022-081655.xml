<?xml version="1.0" encoding="UTF-8"?>
<referentiel_competence specialite="HSE"
                        specialite_long="Hygiène, sécurité, environnement"
                        type="B.U.T." annexe="15"
                        type_structure="type1"
                        type_departement="secondaire"
                        version="2021-12-11 00:00:00"
>
    <competences>
                <competence nom_court="Analyser"
                    numero="1"
                    libelle_long="Analyser les risques professionnels, technologiques et environnementaux"
                    couleur="c1"
                    id="7bd80933168c4716ba2f0d745661bdcd">
            <situations>
                                <situation>en tant que chargé du management des risques en santé sécurité au travail</situation>
                                <situation>en tant que chargé du management des risques pour les installations</situation>
                                <situation>en tant que chargé du management des risques pour les populations</situation>
                                <situation>en tant que chargé du management des risques pour l&#039;environnement</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en utilisant de manière pertinente la réglementation et les autres référentiels appropriés</composante>
                                <composante>en adaptant sa démarche d&#039;analyse au contexte</composante>
                                <composante>en identifiant les dangers inhérents à la situation, leurs sources et leurs cibles</composante>
                                <composante>en employant des méthodes d&#039;analyse des risques (mesures, observations, entretiens) judicieusement choisies</composante>
                                <composante>en déployant des méthodes qualitatives et quantitatives d&#039;évaluation des risques adaptées</composante>
                                <composante>en jugeant de l&#039;acceptabilité des risques dans le contexte donné</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Analyser les risques PTE dans un contexte simple" annee="BUT1">
                <acs>
                                        <ac code="AC11.01">Recueillir des données pertinentes et fiables pour mener l&#039;analyse</ac>
                                        <ac code="AC11.02">Se baser sur la réglementation et un corpus documentaire fiable pour analyser un risque</ac>
                                        <ac code="AC11.03">Analyser la part de subjectivité d&#039;une méthode de cotation des risques</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Analyser les risques PTE d&#039;une structure" annee="BUT2">
                <acs>
                                        <ac code="AC21.01">Utiliser des méthodes de recueil de données adaptées</ac>
                                        <ac code="AC21.02">Vérifier l&#039;évolution des corpus documentaires et réglementaires liés à l&#039;analyse des risques</ac>
                                        <ac code="AC21.03">Justifier les méthodes et les outils d’analyse des risques utilisés</ac>
                                        <ac code="AC21.04">Justifier le résultat de l’évaluation des risques</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Piloter la démarche d&#039;analyse des risques PTE" annee="BUT3">
                <acs>
                                        <ac code="AC31.01">Croiser les méthodes de recueil de données de manière efficiente</ac>
                                        <ac code="AC31.02">Analyser les enjeux et implications de la démarche d&#039;analyse</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Maîtriser"
                    numero="2"
                    libelle_long="Maîtriser les risques professionnels, technologiques et environnementaux (PTE)"
                    couleur="c2"
                    id="07419590bbcb1752c40bc8999495c4dd">
            <situations>
                                <situation>en tant que chargé du management des risques en santé sécurité au travail</situation>
                                <situation>en tant que chargé du management des risques pour les installations</situation>
                                <situation>en tant que chargé du management des risques pour les populations</situation>
                                <situation>en tant que chargé du management des risques pour l&#039;environnement</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en élaborant des mesures de protection en lien avec l&#039;évaluation des risques</composante>
                                <composante>en utilisant de manière pertinente la réglementation, les normes et les procédures HSE appropriées</composante>
                                <composante>en tenant compte des aspects techniques, humains et organisationnels (THO)</composante>
                                <composante>en planifiant des mesures de prévention et de protection des risques PTE adaptées dans le cadre d&#039;une démarche d&#039;amélioration continue</composante>
                                <composante>en mettant en oeuvre judicieusement le plan d&#039;actions défini</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Maîtriser les risques PTE dans un contexte simple" annee="BUT1">
                <acs>
                                        <ac code="AC12.01">Evaluer l&#039;efficacité des actions proposées pour réduire un risque</ac>
                                        <ac code="AC12.02">Utiliser les 3 leviers de maîtrise THO</ac>
                                        <ac code="AC12.03">Planifier les actions en lien avec l&#039;objectif initial</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Maîtriser les risques PTE d&#039;une structure" annee="BUT2">
                <acs>
                                        <ac code="AC22.01">Choisir des mesures de prévention et de protection par rapport à leur efficience</ac>
                                        <ac code="AC22.02">S&#039;appuyer sur des indicateurs adaptés pour suivre l&#039;évolution de la maîtrise des risques</ac>
                                        <ac code="AC22.03">Mener une veille technologique pour mieux maîtriser les risques</ac>
                                        <ac code="AC22.04">Planifier les actions dans la durée</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Piloter la démarche de maîtrise des risques PTE" annee="BUT3">
                <acs>
                                        <ac code="AC32.01">Arbitrer les mesures THO en tenant compte de leur soutenabilité et de leur efficience</ac>
                                        <ac code="AC32.02">Concevoir le plan d&#039;actions dans une logique d&#039;amélioration continue</ac>
                                        <ac code="AC32.03">Appréhender la différence entre l&#039;efficacité escomptée et l&#039;efficacité réelle des mesures mises en place</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Urgences"
                    numero="3"
                    libelle_long="Répondre aux situations d&#039;urgence et de crise"
                    couleur="c3"
                    id="b7cfd49662bb0d199a799146c49f098c">
            <situations>
                                <situation>en tant que chargé du management HSE de l&#039;organisation</situation>
                                <situation>en tant que représentant des autorités compétentes</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en appréhendant l’organisation territoriale des dispositifs d’intervention en cas d’accident</composante>
                                <composante>en appliquant de manière pertinente la réglementation appropriée</composante>
                                <composante>en tenant compte des aspects THO appropriés et des capacités de résilience des organisations</composante>
                                <composante>en communiquant de manière adaptée avec les différents acteurs</composante>
                                <composante>en assurant l’intégrité des personnes, des biens, de l&#039;environnement et des informations vitales</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Organiser et coordonner les interventions d&#039;urgence simples" annee="BUT1">
                <acs>
                                        <ac code="AC13.01">Se baser sur la réglementation et un corpus documentaire fiable pour préparer les plans d&#039;interventions d&#039;urgence simples</ac>
                                        <ac code="AC13.02">Recueillir et diffuser les informations pertinentes auprès des acteurs internes et externes de l&#039;intervention</ac>
                                        <ac code="AC13.03">S’impliquer lors de l&#039;intervention d&#039;urgence</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Organiser et déployer la réponse aux situations d&#039;urgence et de crise" annee="BUT2">
                <acs>
                                        <ac code="AC23.01">Collaborer avec les acteurs internes et externes pour concevoir les plans d&#039;intervention d&#039;urgence</ac>
                                        <ac code="AC23.02">Déployer des procédures de gestion des interventions d&#039;urgence</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Concevoir les réponses et gérer les situations d&#039;urgence et de crise" annee="BUT3">
                <acs>
                                        <ac code="AC33.01">Se baser sur la réglementation et un corpus documentaire fiable pour préparer les plans de gestion de crise</ac>
                                        <ac code="AC33.02">Collaborer avec les acteurs internes et externes pour construire une communication de crise adaptée</ac>
                                        <ac code="AC33.03">Percevoir les enjeux de la mise en place des plans de continuité et de reprise d&#039;activité de la structure</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Animer"
                    numero="4"
                    libelle_long="Animer la démarche Qualité Hygiène Santé Sécurité Environnement (QHSSE)"
                    couleur="c4"
                    id="65dc42675bed9879a5c5e8d0979de21f">
            <situations>
                                <situation>en tant que chargé du management HSE de l&#039;organisation</situation>
                                <situation>en tant que membre d&#039;organismes de conseil</situation>
                                <situation>en tant que membre d&#039;organismes de contrôle</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en s’adaptant à la situation de communication, y compris dans un contexte international</composante>
                                <composante>en promouvant la culture QHSSE</composante>
                                <composante>en s&#039;appuyant sur des réseaux professionnels et des équipes pluridisciplinaires</composante>
                                <composante>en utilisant de manière appropriée les nouvelles technologies de l’information et de la communication</composante>
                                <composante>en mettant en oeuvre la méthodologie et les outils propres à la conduite de projets</composante>
                                <composante>en encourageant une démarche participative et la cohésion de groupe</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Participer à l&#039;animation de la démarche QHSSE" annee="BUT1">
                <acs>
                                        <ac code="AC14.01">Adopter une attitude en cohérence avec les valeurs QHSSE</ac>
                                        <ac code="AC14.02">Participer de manière adéquate aux réunions et/ou aux animations de formation/sensibilisation</ac>
                                        <ac code="AC14.03">Communiquer de manière adaptée à la cible, à l&#039;écrit et à l&#039;oral</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Déployer la démarche QHSSE" annee="BUT2">
                <acs>
                                        <ac code="AC24.01">Promouvoir par ses actions les valeurs QHSSE dans son environnement professionnel</ac>
                                        <ac code="AC24.02">Préparer et conduire des réunions et des actions de formation</ac>
                                        <ac code="AC24.03">Créer des supports de communication adaptés à l&#039;information à diffuser et à la cible</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Concevoir et organiser la démarche QHSSE" annee="BUT3">
                <acs>
                                        <ac code="AC34.01">Impulser et coordonner une démarche QHSSE collaborative</ac>
                                        <ac code="AC34.02">Percevoir les forces et faiblesses d&#039;un plan de communication QHSSE</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Manager le QHSSE"
                    numero="5"
                    libelle_long="Accompagner la direction dans son management QHSSE"
                    couleur="c5"
                    id="05298dc15e470bfcb307395d36ff4a54">
            <situations>
                                <situation>en tant que chargé du management HSE de l&#039;organisation</situation>
                                <situation>en tant que membre d&#039;organismes de conseil</situation>
                                <situation>en tant que membre d&#039;organismes de contrôle</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en intégrant les principes du développement durable (DD) et de la responsabilité sociétale (RSE/RSO)</composante>
                                <composante>en utilisant une démarche d&#039;amélioration continue</composante>
                                <composante>en assurant une veille technique et règlementaire pertinente</composante>
                                <composante>en mettant en place de manière appropriée le système de management intégré et/ou de la sûreté</composante>
                                <composante>en tenant compte des aspects organisationnels, humains, techniques et économiques</composante>
                                <composante>en rendant compte des résultats obtenus en QHSSE de manière structurée et transparente</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Utiliser un système de management intégré QHSSE" annee="BUT2">
                <acs>
                                        <ac code="AC25.01">Analyser le contexte et l&#039;environnement de l&#039;organisation dans sa globalité</ac>
                                        <ac code="AC25.02">Alerter la direction sur ses responsabilités en matière de management QHSSE</ac>
                                        <ac code="AC25.03">Evaluer l&#039;efficacité de la politique de management des risques en s&#039;appuyant sur des indicateurs adaptés</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Mettre en place un système de management intégré QHSSE" annee="BUT3">
                <acs>
                                        <ac code="AC35.01">Agir en tenant compte de la composition et de la complexité de fonctionnement des différentes instances</ac>
                                        <ac code="AC35.02">Tirer partie des fonctions et de la complémentarité des outils de management (référentiels, normes, audits, etc...)</ac>
                                        <ac code="AC35.03">Promouvoir auprès de la direction une démarche volontariste de management des risques</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
            </competences>
    <parcours>
                <parcour numero="0" libelle="Science du danger et management des risques professionnels, technologiques et environnementaux" code="SDMRPTE">
                        <annee ordre="1">
                                <competence niveau="1" id="7bd80933168c4716ba2f0d745661bdcd"/>
                                <competence niveau="1" id="07419590bbcb1752c40bc8999495c4dd"/>
                                <competence niveau="1" id="b7cfd49662bb0d199a799146c49f098c"/>
                                <competence niveau="1" id="65dc42675bed9879a5c5e8d0979de21f"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="7bd80933168c4716ba2f0d745661bdcd"/>
                                <competence niveau="2" id="07419590bbcb1752c40bc8999495c4dd"/>
                                <competence niveau="2" id="b7cfd49662bb0d199a799146c49f098c"/>
                                <competence niveau="2" id="65dc42675bed9879a5c5e8d0979de21f"/>
                                <competence niveau="1" id="05298dc15e470bfcb307395d36ff4a54"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="7bd80933168c4716ba2f0d745661bdcd"/>
                                <competence niveau="3" id="07419590bbcb1752c40bc8999495c4dd"/>
                                <competence niveau="3" id="b7cfd49662bb0d199a799146c49f098c"/>
                                <competence niveau="3" id="65dc42675bed9879a5c5e8d0979de21f"/>
                                <competence niveau="2" id="05298dc15e470bfcb307395d36ff4a54"/>
                            </annee>
                    </parcour>
            </parcours>
</referentiel_competence>
