<?xml version="1.0" encoding="UTF-8"?>
<referentiel_competence specialite="MP"
                        specialite_long="Mesures physiques"
                        type="B.U.T." annexe="18"
                        type_structure="type2"
                        type_departement="secondaire"
                        version="2021-12-11 00:00:00"
>
    <competences>
                <competence nom_court="Mener"
                    numero="1"
                    libelle_long="Mener une campagne de mesures"
                    couleur="c1"
                    id="9b2af9433f588eed279f48fd030081ab">
            <situations>
                                <situation>dans un contexte de production en milieu industriel et en laboratoire  </situation>
                                <situation>dans un contexte de recherche et développement en milieu industriel et en laboratoire</situation>
                                <situation>dans une démarche de qualification ou de certification</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en choisissant une démarche scientifique adaptée</composante>
                                <composante>en élaborant un protocole adapté, qui respecte les règles de sécurité et les normes en vigueur</composante>
                                <composante>en collectant les données de manière fiable</composante>
                                <composante>en traitant les données de manière pertinente</composante>
                                <composante>en présentant les résultats de mesures selon les normes en vigueur</composante>
                                <composante>en analysant les résultats pour mettre en place d&#039;éventuelles actions correctives</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Mener une campagne de mesures pour un nombre restreint de grandeurs" annee="BUT1">
                <acs>
                                        <ac code="AC11.01">Identifier le phénomène physique à l’origine d’une mesure et réaliser un protocole pour une mesure simple</ac>
                                        <ac code="AC11.02">Effectuer les mesures en respectant les règles de sécurité et normes en vigueur</ac>
                                        <ac code="AC11.03">Vérifier la cohérence des mesures avec les résultats attendus, effectuer une action corrective le cas échéant</ac>
                                        <ac code="AC11.04">Présenter un résultat de mesures avec les outils appropriés (numérique, tableau, graphique...)</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Mener une campagne de mesures multiples nécessitant un traitement complexe des données" annee="BUT2">
                <acs>
                                        <ac code="AC21.01">Modéliser un problème en lien avec des lois physiques ou chimiques</ac>
                                        <ac code="AC21.02">Elaborer un protocole pour plusieurs mesures s&#039;appuyant sur les règles de sécurité et les normes en vigueur</ac>
                                        <ac code="AC21.03">Traiter les valeurs mesurées : fiabilité, traçabilité, archivage des données, analyses statistiques, ...</ac>
                                        <ac code="AC21.04">Présenter les résultats de mesures dans un format adapté aux objectifs</ac>
                                        <ac code="AC21.05">Présenter à l&#039;oral les caractéristiques de la campagne de mesures et ses conclusions</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Mener une campagne de mesures dans un contexte professionnel spécifique" annee="BUT3">
                <acs>
                                        <ac code="AC31.01">Elaborer, améliorer et valider un protocole dans un contexte professionnel spécifique</ac>
                                        <ac code="AC31.02">Identifier les contraintes réglementaires et les spécificités rencontrées dans ce contexte spécifique</ac>
                                        <ac code="AC31.03">Utiliser des outils mathématiques et logiciels métiers adaptés au contexte spécifique pour le post-traitement des valeurs mesurées</ac>
                                        <ac code="AC31.04">Présenter à l&#039;écrit et à l&#039;oral en français et en anglais un rapport de mesures adapté au contexte spécifique</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Déployer"
                    numero="2"
                    libelle_long="Déployer la métrologie et la démarche qualité"
                    couleur="c2"
                    id="52188cb32a2bbabb766216066b4490ce">
            <situations>
                                <situation>dans un service métrologie</situation>
                                <situation>dans un service qualité </situation>
                                <situation>dans une entreprise d&#039;expertise en métrologie</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en exprimant le résultat avec son incertitude de mesure selon les normes en vigueur</composante>
                                <composante>en choisissant et mettant en oeuvre l&#039;instrument de mesure en fonction de ses caractéristiques 
métrologiques</composante>
                                <composante>en utilisant des outils statistiques adaptés pour l&#039;analyse des mesures et leur comparaison</composante>
                                <composante>en s&#039;assurant du respect des procédures (traçabilité, fiabilité)</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Déployer la métrologie et la démarche qualité pour un résultat de mesure" annee="BUT1">
                <acs>
                                        <ac code="AC12.01">Identifier les éléments de langage liés à la métrologie</ac>
                                        <ac code="AC12.02">Evaluer une incertitude de mesure </ac>
                                        <ac code="AC12.03">Présenter correctement un résultat de mesure, avec son unité et son incertitude</ac>
                                        <ac code="AC12.04">Etalonner un appareil de mesure</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Déployer la métrologie et la démarche qualité pour un instrument de mesure" annee="BUT2">
                <acs>
                                        <ac code="AC22.01">Evaluer la conformité, gérer la non conformité</ac>
                                        <ac code="AC22.02">Mettre en place un suivi métrologique pour un instrument de mesure</ac>
                                        <ac code="AC22.03">Rédiger les procédures métrologiques et fiches de suivi</ac>
                                        <ac code="AC22.04">Mettre en place des procédures qualité pour un instrument</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Déployer la métrologie et la démarche qualité pour un parc d&#039;instruments" annee="BUT3">
                <acs>
                                        <ac code="AC32.01">Faire évoluer des procédures qualité</ac>
                                        <ac code="AC32.02">Gérer un parc d&#039;instruments dans une démarche qualité</ac>
                                        <ac code="AC32.03">Préparer les éléments d&#039;un audit qualité</ac>
                                        <ac code="AC32.04">Réaliser une veille technologique ou normative</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Mettre en œuvre"
                    numero="3"
                    libelle_long="Mettre en œuvre une chaîne de mesure et d&#039;instrumentation"
                    couleur="c3"
                    id="27ae1b6b36d0fb432cb03d762de7ea98">
            <situations>
                                <situation>dans un contexte de production en milieu industriel et en laboratoire</situation>
                                <situation>dans un contexte de recherche et développement en milieu industriel et en laboratoire</situation>
                                <situation>en milieu ou conditions à contraintes spécifiques</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en choisissant les capteurs ou détecteurs les mieux adaptés</composante>
                                <composante>en choisissant un transfert et traitement analogique ou numérique du signal adaptés</composante>
                                <composante>en mettant en place le pilotage d&#039;une chaîne de mesure avec ou sans régulation</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Mettre en œuvre une chaîne de mesure simple, piloter un instrument de façon élémentaire" annee="BUT1">
                <acs>
                                        <ac code="AC13.01">Identifier des couples capteurs/conditionneurs selon la mesure demandée</ac>
                                        <ac code="AC13.02">Acquérir et numériser des signaux analogiques</ac>
                                        <ac code="AC13.03">Choisir un instrument de mesure adapté au signal </ac>
                                        <ac code="AC13.04">Traiter avec ou sans régulation un signal analogique</ac>
                                        <ac code="AC13.05">Concevoir un algorithme pour le traitement des données ou le pilotage d&#039;un instrument</ac>
                                        <ac code="AC13.06">Utiliser un langage de programmation permettant la mise en place d&#039;un algorithme </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Mettre en œuvre une chaîne d&#039;instrumentation simple pouvant associer mesure, régulation et pilotage" annee="BUT2">
                <acs>
                                        <ac code="AC23.01">Mettre en œuvre le conditionnement d&#039;un signal issu d&#039;un capteur </ac>
                                        <ac code="AC23.02">Mettre en œuvre des techniques simples d&#039;amélioration du rapport signal sur bruit</ac>
                                        <ac code="AC23.03">Réguler des systèmes analogiques ou numériques </ac>
                                        <ac code="AC23.04">Echanger des données entre un instrument de mesure et un ordinateur</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Mettre en œuvre une chaîne d&#039;instrumentation complexe. Prendre en compte des conditions spécifiques ou extrêmes." annee="BUT3">
                <acs>
                                        <ac code="AC33.01">Choisir un mode de transfert de données adapté   </ac>
                                        <ac code="AC33.02">Mettre en œuvre des systèmes de mesures en réseau</ac>
                                        <ac code="AC33.03">Mettre en œuvre des techniques d&#039;extraction et d&#039;exploitation d&#039;un signal bruité</ac>
                                        <ac code="AC33.04">Choisir les éléments de la chaîne de mesure face à des conditions extrêmes</ac>
                                        <ac code="AC33.05">Contrôler à distance un système de mesures embarqué</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Caractériser"
                    numero="4"
                    libelle_long="Caractériser des grandeurs physiques, chimiques et les propriétés d&#039;un matériau"
                    couleur="c4"
                    id="0fe72359d140936fdd5d0475696766c2">
            <situations>
                                <situation>dans un contexte de production en milieu industriel et en laboratoire </situation>
                                <situation>dans un contexte de recherche et développement en milieu industriel et en laboratoire</situation>
                                <situation> en milieu ou conditions à contraintes spécifiques</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en identifiant les grandeurs physiques et chimiques pertinentes</composante>
                                <composante>en adaptant la préparation de l&#039;échantillon à la mesure</composante>
                                <composante>en tenant compte de l&#039;état, de la structure de la matière et du type de matériau</composante>
                                <composante>en mettant en oeuvre les outils et techniques de caractérisation adaptés</composante>
                                <composante>en analysant les résultats en relation avec la structure des matériaux</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Caractériser des grandeurs physiques, chimiques et les propriétés d&#039;un matériau en utilisant des méthodes simples" annee="BUT1">
                <acs>
                                        <ac code="AC14.01">Identifier et comprendre les édifices atomiques et moléculaires</ac>
                                        <ac code="AC14.02">Appréhender les aspects énergétiques de la matière</ac>
                                        <ac code="AC14.03">Mettre en œuvre des outils d&#039;analyses et de caractérisation physique et chimique en respectant les bonnes pratiques de laboratoire</ac>
                                        <ac code="AC14.04">Identifier des types de réaction chimique et mesurer leur avancement</ac>
                                        <ac code="AC14.05">Identifier les différentes classes de matériaux</ac>
                                        <ac code="AC14.06">Relier les différentes propriétés d&#039;un matériau à sa structure</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Caractériser des grandeurs physico-chimiques et les propriétés d&#039;un matériau en utilisant des méthodes complexes" annee="BUT2">
                <acs>
                                        <ac code="AC24.01">Appliquer les principes et mettre en œuvre des techniques d’analyses chimique et physique</ac>
                                        <ac code="AC24.02">Analyser, interpréter, exploiter les résultats d’analyses et de caractérisations</ac>
                                        <ac code="AC24.03"> Appliquer les principes et mettre en œuvre des techniques d&#039;analyses et de caractérisations de la structure et des propriétés des matériaux</ac>
                                        <ac code="AC24.04"> Mettre en œuvre des techniques de contrôle non destructif</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Caractériser des propriétés physico-chimiques de produits et de matériaux complexes" annee="BUT3">
                <acs>
                                        <ac code="AC34.01">Mettre en œuvre la caractérisation structurale, texturale et de surface de matériauxs </ac>
                                        <ac code="AC34.02">Mettre en œuvre la caractérisation de matériaux complexes (composites, nanocomposites, microstructurés, nanostructurés) </ac>
                                        <ac code="AC34.03">Concevoir et mettre en œuvre une démarche globale de caractérisation à l&#039;aide de différentes technique</ac>
                                        <ac code="AC34.04">Optimiser un procédé et une technique de contrôle pour un contexte industriel particulier</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Définir"
                    numero="5"
                    libelle_long="Définir un cahier des charges de mesures dans une démarche environnementale"
                    couleur="c5"
                    id="c1aad6cd755bacb9d12ca605c480cdf1">
            <situations>
                                <situation>dans un contexte de production en milieu industriel et en laboratoire   </situation>
                                <situation>dans un contexte de recherche et développement en milieu industriel et en laboratoire</situation>
                                <situation>dans une démarche de qualification ou de certification</situation>
                                <situation>dans le cadre de mesures in situ pour le contrôle et la surveillance de l&#039;environnement</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en limitant les impacts sur l&#039;environnement et les impacts énergétiques
en mettant</composante>
                                <composante>en mettant en oeuvre une gestion de projet pertinente</composante>
                                <composante>en choisissant les moyens techniques et métrologiques adaptés</composante>
                                <composante>en utilisant une communication adaptée</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Définir un cahier des charges d&#039;une mesure simple dans une démarche environnementale" annee="BUT1">
                <acs>
                                        <ac code="AC15.01">Conduire une recherche documentaire</ac>
                                        <ac code="AC15.02">Identifier les éléments nécessaires pour une étude HSE</ac>
                                        <ac code="AC15.03">Réaliser des contrôles environnementaux simples</ac>
                                        <ac code="AC15.04">Organiser un projet et son déroulement</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Définir un cahier des charges d&#039;un ensemble de mesures dans une démarche environnementale" annee="BUT2">
                <acs>
                                        <ac code="AC25.01">Lister et évaluer les contraintes pour un ensemble de mesures  </ac>
                                        <ac code="AC25.02">Evaluer l&#039;impact environnemental dans le cadre de mesures</ac>
                                        <ac code="AC25.03"> Sélectionner des techniques pour des mesures environnementales</ac>
                                        <ac code="AC25.04">Réaliser des contrôles environnementaux complexes</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Définir un cahier des charges de mesures répondant à de fortes contraintes environnementales ou énergétiques" annee="BUT3">
                <acs>
                                        <ac code="AC35.01">Valider des méthodes de mesures environnementales ou énergétiques pour répondre à des normes spécifiques  </ac>
                                        <ac code="AC35.02">Concevoir des méthodes d&#039;analyse en lien avec le développement durable ou l&#039;environnement</ac>
                                        <ac code="AC35.03">Optimiser des méthodes d&#039;analyse en prenant en compte des contraintes environnementales ou énergétiques fortes</ac>
                                        <ac code="AC35.04">Piloter un projet de mesures répondant à de fortes contraintes environnementales ou énergétiques</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
            </competences>
    <parcours>
                <parcour numero="0" libelle="Techniques d&#039;instrumentation" code="TI">
                        <annee ordre="1">
                                <competence niveau="1" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="1" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="1" id="27ae1b6b36d0fb432cb03d762de7ea98"/>
                                <competence niveau="1" id="0fe72359d140936fdd5d0475696766c2"/>
                                <competence niveau="1" id="c1aad6cd755bacb9d12ca605c480cdf1"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="2" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="2" id="27ae1b6b36d0fb432cb03d762de7ea98"/>
                                <competence niveau="2" id="0fe72359d140936fdd5d0475696766c2"/>
                                <competence niveau="2" id="c1aad6cd755bacb9d12ca605c480cdf1"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="3" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="3" id="27ae1b6b36d0fb432cb03d762de7ea98"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="Matériaux et Contrôles Physico-Chimiques" code="MCPC">
                        <annee ordre="1">
                                <competence niveau="1" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="1" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="1" id="27ae1b6b36d0fb432cb03d762de7ea98"/>
                                <competence niveau="1" id="0fe72359d140936fdd5d0475696766c2"/>
                                <competence niveau="1" id="c1aad6cd755bacb9d12ca605c480cdf1"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="2" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="2" id="27ae1b6b36d0fb432cb03d762de7ea98"/>
                                <competence niveau="2" id="0fe72359d140936fdd5d0475696766c2"/>
                                <competence niveau="2" id="c1aad6cd755bacb9d12ca605c480cdf1"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="3" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="3" id="0fe72359d140936fdd5d0475696766c2"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="Mesures et Analyses Environnementales" code="MAE">
                        <annee ordre="1">
                                <competence niveau="1" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="1" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="1" id="27ae1b6b36d0fb432cb03d762de7ea98"/>
                                <competence niveau="1" id="0fe72359d140936fdd5d0475696766c2"/>
                                <competence niveau="1" id="c1aad6cd755bacb9d12ca605c480cdf1"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="2" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="2" id="27ae1b6b36d0fb432cb03d762de7ea98"/>
                                <competence niveau="2" id="0fe72359d140936fdd5d0475696766c2"/>
                                <competence niveau="2" id="c1aad6cd755bacb9d12ca605c480cdf1"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="9b2af9433f588eed279f48fd030081ab"/>
                                <competence niveau="3" id="52188cb32a2bbabb766216066b4490ce"/>
                                <competence niveau="3" id="c1aad6cd755bacb9d12ca605c480cdf1"/>
                            </annee>
                    </parcour>
            </parcours>
</referentiel_competence>
