# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaire ajout d'une "assiduité" sur un étudiant
Formulaire ajout d'un justificatif sur un étudiant
"""

from flask_wtf import FlaskForm
from flask_wtf.file import MultipleFileField
from wtforms import (
    BooleanField,
    SelectField,
    StringField,
    SubmitField,
    RadioField,
    TextAreaField,
    validators,
)
from wtforms.validators import DataRequired
from app.scodoc import sco_utils as scu


class AjoutAssiOrJustForm(FlaskForm):
    """Elements communs aux deux formulaires ajout
    assiduité et justificatif
    """

    def __init__(self, *args, **kwargs):
        "Init form, adding a filed for our error messages"
        super().__init__(*args, **kwargs)
        self.ok = True
        self.error_messages: list[str] = []  # used to report our errors

    def set_error(self, err_msg, field=None):
        "Set error message both in form and field"
        self.ok = False
        self.error_messages.append(err_msg)
        if field:
            field.errors.append(err_msg)

    date_debut = StringField(
        "Date de début",
        validators=[validators.Length(max=10)],
        render_kw={
            "class": "datepicker",
            "size": 10,
            "id": "assi_date_debut",
        },
    )
    heure_debut = StringField(
        "Heure début",
        default="",
        validators=[validators.Length(max=5)],
        render_kw={
            "class": "timepicker",
            "size": 5,
            "id": "assi_heure_debut",
        },
    )
    heure_fin = StringField(
        "Heure fin",
        default="",
        validators=[validators.Length(max=5)],
        render_kw={
            "class": "timepicker",
            "size": 5,
            "id": "assi_heure_fin",
        },
    )
    date_fin = StringField(
        "Date de fin (si plusieurs jours)",
        validators=[validators.Length(max=10)],
        render_kw={
            "class": "datepicker",
            "size": 10,
            "id": "assi_date_fin",
        },
    )

    entry_date = StringField(
        "Date de dépot ou saisie",
        validators=[validators.Length(max=10)],
        render_kw={
            "class": "datepicker",
            "size": 10,
            "id": "entry_date",
        },
    )
    submit = SubmitField("Enregistrer")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})


class AjoutAssiduiteEtudForm(AjoutAssiOrJustForm):
    "Formulaire de saisie d'une assiduité pour un étudiant"
    description = TextAreaField(
        "Description",
        render_kw={
            "id": "description",
            "cols": 75,
            "rows": 4,
            "maxlength": 500,
        },
    )
    assi_etat = RadioField(
        "Signaler:",
        choices=[("absent", "absence"), ("retard", "retard"), ("present", "présence")],
        default="absent",
        validators=[
            validators.DataRequired("spécifiez le type d'évènement à signaler"),
        ],
    )
    modimpl = SelectField(
        "Module",
        choices={},  # will be populated dynamically
    )
    est_just = BooleanField("Justifiée")


class AjoutJustificatifEtudForm(AjoutAssiOrJustForm):
    "Formulaire de saisie d'un justificatif pour un étudiant"
    raison = TextAreaField(
        "Raison",
        render_kw={
            "id": "raison",
            "cols": 75,
            "rows": 4,
            "maxlength": 500,
        },
    )
    etat = SelectField(
        "État du justificatif",
        choices=[
            ("", "Choisir..."),  # Placeholder
            (scu.EtatJustificatif.ATTENTE.value, "En attente de validation"),
            (scu.EtatJustificatif.NON_VALIDE.value, "Non valide"),
            (scu.EtatJustificatif.MODIFIE.value, "Modifié"),
            (scu.EtatJustificatif.VALIDE.value, "Valide"),
        ],
        validators=[DataRequired(message="This field is required.")],
    )
    fichiers = MultipleFileField(label="Ajouter des fichiers")


class ChoixDateForm(FlaskForm):
    def __init__(self, *args, **kwargs):
        "Init form, adding a filed for our error messages"
        super().__init__(*args, **kwargs)
        self.ok = True
        self.error_messages: list[str] = []  # used to report our errors

    def set_error(self, err_msg, field=None):
        "Set error message both in form and field"
        self.ok = False
        self.error_messages.append(err_msg)
        if field:
            field.errors.append(err_msg)

    date = StringField(
        "Date",
        validators=[validators.Length(max=10)],
        render_kw={
            "class": "datepicker",
            "size": 10,
            "id": "date",
        },
    )
    submit = SubmitField("Enregistrer")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
