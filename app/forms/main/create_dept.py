# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaires création département
"""

from flask_wtf import FlaskForm
from wtforms import SubmitField, validators
from wtforms.fields.simple import StringField, BooleanField

from app.models import SHORT_STR_LEN


class CreateDeptForm(FlaskForm):
    """Formulaire permettant l'ajout d'un département"""

    acronym = StringField(
        label="Acronyme",
        validators=[
            validators.regexp(
                r"^[a-zA-Z0-9_\-]*$",
                message="Ne doit comporter que lettres, chiffres ou -",
            ),
            validators.Length(
                max=SHORT_STR_LEN,
                message=f"L'acronyme ne doit pas dépasser {SHORT_STR_LEN} caractères",
            ),
            validators.DataRequired("acronyme du département requis"),
        ],
    )
    # description = StringField(label="Description")
    visible = BooleanField(
        "Visible sur page d'accueil",
        default=True,
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
