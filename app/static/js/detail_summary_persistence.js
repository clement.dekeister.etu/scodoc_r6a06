/*
Ce module a pour objet de conserver la configuration (état ouvert/fermé) des balises details/summary
d'un page donnée.
On stocke l'état de chaque balise dans le localstorage javascript (associé à la page)
pour cela il est nécessaire d'identifier chaque balise. Ceci se fait:
* soit au moyen de l'id si un id est déclaré pour la balise ('#' + id)
* soit en numérotant les balises dans l'ordre de parcours de la page. ('ds_' + numéro)
l'identifiant généré est stocké comme attribut ds_id de la balise.

la variable keepDetails peut être mise à false (par inclusion du code
<script type="text/javascript>keepDetails = false;</script> pour reinitialisés l'état de toutes
les balises (fermées par défaut sauf si attribut open déjà activé dans le code source de la page)

*/

const ID_ATTRIBUTE = "ds_id";

function genere_id(detail, idnum) {
  let id = "ds_" + idnum;
  if (detail.getAttribute("id")) {
    id = "#" + detail.getAttribute("id");
  }
  detail.setAttribute(ID_ATTRIBUTE, id);
  return id;
}

// remise à l'état initial. doit être exécuté dès le chargement de la page pour que l'état 'open'
// des balises soit celui indiqué par le serveur (et donc indépendant du localstorage)
function reset_detail(detail, id) {
  let opened = detail.getAttribute("open");
  if (opened) {
    detail.setAttribute("open", true);
    localStorage.setItem(id, true);
  } else {
    detail.removeAttribute("open");
    localStorage.setItem(id, false);
  }
}

function restore_detail(detail, id) {
  let status = localStorage.getItem(id);
  if (status == "true") {
    detail.setAttribute("open", true);
  } else {
    detail.removeAttribute("open");
  }
}

function add_listener(detail) {
  detail.addEventListener("toggle", (e) => {
    let id = e.target.getAttribute(ID_ATTRIBUTE);
    let ante = e.target.getAttribute("open");
    if (ante == null) {
      localStorage.setItem(id, false);
    } else {
      localStorage.setItem(id, true);
    }
    e.stopPropagation();
  });
}

function reset_ds() {
  let idnum = 0;
  keepDetails = true;
  details = document.querySelectorAll("details");
  details.forEach(function (detail) {
    let id = genere_id(detail, idnum);
    console.log("Processing " + id);
    if (keepDetails) {
      restore_detail(detail, id);
    } else {
      reset_detail(detail, id);
    }
    add_listener(detail);
    idnum++;
  });
}

window.addEventListener("load", function () {
  console.log("details/summary persistence ON");
  reset_ds();
});
