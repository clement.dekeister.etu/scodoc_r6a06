// active les menus des codes "manuels" (année, RCUEs)
function enable_manual_codes(elt) {
  $(".jury_but select.manual").prop("disabled", !elt.checked);
}

// changement d'un menu code:
function change_menu_code(elt) {
  // Ajuste styles pour visualiser codes enregistrés/modifiés
  if (elt.value != elt.dataset.orig_code) {
    elt.parentElement.parentElement.classList.add("modified");
  } else {
    elt.parentElement.parentElement.classList.remove("modified");
  }
  if (elt.value == elt.dataset.orig_recorded) {
    elt.parentElement.parentElement.classList.add("recorded");
  } else {
    elt.parentElement.parentElement.classList.remove("recorded");
  }
  // Si RCUE passant en ADJ, change les menus des UEs associées ADJR
  if (
    elt.classList.contains("code_rcue") &&
    elt.dataset.niveau_id &&
    elt.value == "ADJ" &&
    elt.value != elt.dataset.orig_recorded
  ) {
    let ue_selects =
      elt.parentElement.parentElement.parentElement.querySelectorAll(
        "select.ue_rcue_" + elt.dataset.niveau_id
      );
    ue_selects.forEach((select) => {
      if (select.value != "ADM") {
        select.value = "ADJR";
        change_menu_code(select); // pour changer les styles
      }
    });
  }
}

$(function () {
  // Recupère la liste ordonnées des etudids
  // pour avoir le "suivant" et le "précédent"
  // (liens de navigation)
  const url = new URL(document.URL);
  const frags = url.pathname.split("/"); // .../formsemestre_validation_but/formsemestre_id/etudid
  const etudid = frags[frags.length - 1];
  const formsemestre_id = frags[frags.length - 2];
  const etudids_key = JSON.stringify(["etudids", url.origin, formsemestre_id]);
  const etudids_str = localStorage.getItem(etudids_key);
  const noms_key = JSON.stringify(["noms", url.origin, formsemestre_id]);
  const noms_str = localStorage.getItem(noms_key);
  if (etudids_str && noms_str) {
    const etudids = JSON.parse(etudids_str);
    const noms = JSON.parse(noms_str);
    const cur_idx = etudids.indexOf(etudid);
    let prev_idx = -1;
    let next_idx = -1;
    if (cur_idx != -1) {
      if (cur_idx > 0) {
        prev_idx = cur_idx - 1;
      }
      if (cur_idx < etudids.length - 1) {
        next_idx = cur_idx + 1;
      }
    }
    if (prev_idx != -1) {
      let elem = document.querySelector("div.prev a");
      if (elem) {
        elem.href = elem.href.replace("PREV", etudids[prev_idx]);
        elem.innerHTML = noms[prev_idx];
      }
    } else {
      document.querySelector("div.prev").innerHTML = "";
    }
    if (next_idx != -1) {
      let elem = document.querySelector("div.next a");
      if (elem) {
        elem.href = elem.href.replace("NEXT", etudids[next_idx]);
        elem.innerHTML = noms[next_idx];
      }
    } else {
      document.querySelector("div.next").innerHTML = "";
    }
  } else {
    // Supprime les liens de navigation
    document.querySelector("div.prev").innerHTML = "";
    document.querySelector("div.next").innerHTML = "";
  }
});

//  ----- Etat du formulaire jury pour éviter sortie sans enregistrer
let FORM_STATE = "";
let IS_SUBMITTING = false;

// Une chaine décrivant l'état du form
function get_form_state() {
  let codes = [];
  // il n'y a que des <select>
  document.querySelectorAll("select").forEach((sel) => codes.push(sel.value));
  return codes.join();
}

$("document").ready(function () {
  FORM_STATE = get_form_state();
  document
    .querySelector("form#jury_but")
    .addEventListener("submit", jury_form_submit);
});

function is_modified() {
  return FORM_STATE != get_form_state();
}

function jury_form_submit(event) {
  IS_SUBMITTING = true;
}

window.addEventListener("beforeunload", function (e) {
  if (!IS_SUBMITTING && is_modified()) {
    var confirmationMessage = "Changements non enregistrés !";
    (e || window.event).returnValue = confirmationMessage;
    return confirmationMessage;
  }
});
