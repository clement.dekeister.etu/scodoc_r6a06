"""
Commande permettant de supprimer les assiduités et les justificatifs

Ecrit par Matthias HARTMANN
"""
import sqlalchemy as sa

from app import db
from app.models import Justificatif, Assiduite, Departement
from app.scodoc.sco_archives_justificatifs import JustificatifArchiver
from app.scodoc.sco_utils import TerminalColor


def downgrade_module(
    dept: str = None, assiduites: bool = False, justificatifs: bool = False
):
    """
    Supprime les assiduités et/ou justificatifs du dept sélectionné ou de tous les départements

    Args:
        dept (str, optional): l'acronym du département. Par défaut tous les départements.
        assiduites (bool, optional): suppression des assiduités. Par défaut : Non
        justificatifs (bool, optional): supression des justificatifs. Par défaut : Non
    """

    dept_etudid: list[int] = None
    dept_id: int = None

    # Récupération du département si spécifié
    if dept is not None:
        departement: Departement = Departement.query.filter_by(acronym=dept).first()

        assert departement is not None, "Le département n'existe pas."

        dept_etudid = [etud.id for etud in departement.etudiants]
        dept_id = departement.id

    # Suppression des assiduités
    if assiduites:
        _remove_assiduites(dept_etudid)
    # Suppression des justificatifs
    if justificatifs:
        _remove_justificatifs(dept_etudid)
        _remove_justificatifs_archive(dept_id)

    # Si on supprime tout le module assiduité/justificatif alors on remet à zero
    # les séquences postgres
    if dept is None:
        if assiduites:
            db.session.execute(
                sa.text("ALTER SEQUENCE assiduites_id_seq RESTART WITH 1")
            )
        if justificatifs:
            db.session.execute(
                sa.text("ALTER SEQUENCE justificatifs_id_seq RESTART WITH 1")
            )

    # On valide l'opération sur la bdd
    db.session.commit()

    # On affiche un message pour l'utilisateur
    print(
        f"{TerminalColor.GREEN}Le module assiduité a bien été remis à zero.{TerminalColor.RESET}"
    )


def _remove_assiduites(dept_etudid: str = None):
    """
    _remove_assiduites Supprime les assiduités

    Args:
        dept_etudid (str, optional): la liste des etudid d'un département. Defaults to None.
    """
    if dept_etudid is None:
        # Si pas d'étudids alors on supprime toutes les assiduités
        Assiduite.query.delete()
    else:
        # Sinon on supprime que les assiduités des étudiants donnés
        Assiduite.query.filter(Assiduite.etudid.in_(dept_etudid)).delete()


def _remove_justificatifs(dept_etudid: str = None):
    """
    _remove_justificatifs Supprime les justificatifs

    Args:
        dept_etudid (str, optional): la liste des etudid d'un département. Defaults to None.
    """
    if dept_etudid is None:
        # Si pas d'étudids alors on supprime tous les justificatifs
        Justificatif.query.delete()
    else:
        # Sinon on supprime que les justificatifs des étudiants donnés
        Justificatif.query.filter(Justificatif.etudid.in_(dept_etudid)).delete()


def _remove_justificatifs_archive(dept_id: int = None):
    """
    _remove_justificatifs_archive On supprime les archives des fichiers justificatifs

    Args:
        dept_id (int, optional): l'id du département à supprimer . Defaults to None.
            Si none : supprime tous les département
            Sinon uniquement le département sélectionné
    """
    JustificatifArchiver().remove_dept_archive(dept_id)
